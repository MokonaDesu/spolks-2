import socket
import struct

SIMPLE_TEXT = 0x00
CLIENT_CMD = 0x01
IP_ECHO = 0x01
IP_ECHO_RESPONSE = 0x02

class ChatClient:
    def __init__(self, primary_iface, port = 27015, mcast_port = 27016, ttl = 64):
        self.__primary_iface = primary_iface
        self.__chat_port = port
        self.__mcast_port = mcast_port
        self.__ttl = ttl
        self.__buffer_size = 8192
        self.__open_broadcast_socket()
        self.__joined_groups = { }
        self.__current_group = None

    def __open_mcast_socket(self, group):
        mcast_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        mcast_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
        mcast_socket.setsockopt(socket.SOL_IP, socket.IP_MULTICAST_TTL, self.__ttl)
        mcast_socket.setblocking(False)
        port = self.__mcast_port
        mcast_socket.bind((group, port))
        #self.__mcast_port += 1
        return port, mcast_socket

    def __open_broadcast_socket(self):
        self.__chat_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        self.__chat_socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, True)
        self.__chat_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
        self.__chat_socket.setsockopt(socket.SOL_IP, socket.IP_TTL, self.__ttl)
        self.__chat_socket.setsockopt(socket.SOL_IP, socket.IP_MULTICAST_TTL, self.__ttl)
        self.__chat_socket.setblocking(False)
        self.__chat_socket.bind(('', self.__chat_port))

    def __get_broadcast_addr(self):
        if self.__current_group == None:
            return (self.__primary_iface['bcast'], self.__chat_port)
        else:
            return (self.__current_group, self.__joined_groups[self.__current_group][0])

    def send_msg(self, msg):
        self.__chat_socket.sendto(chr(SIMPLE_TEXT) + msg, self.__get_broadcast_addr())
    
    def send_cmd(self, cmd):
        self.__chat_socket.sendto(chr(CLIENT_CMD) + chr(cmd), self.__get_broadcast_addr())

    def __process_client_cmd(self, cmd, addr, socket_to_process):
        cmd = ord(cmd[0])
        if cmd == IP_ECHO:
            socket_to_process.sendto(chr(CLIENT_CMD) + chr(IP_ECHO_RESPONSE), addr)
        if cmd == IP_ECHO_RESPONSE:
            return { 'contents': 'I am online! My IP address is %s' % addr[0], 'sender': addr }

    def switch_group(self, group):
        if group == 'all':
            self.__current_group = None
            return 'OK'
        elif group in self.__joined_groups:
            self.__current_group = group
            return 'OK'
        else:
            return 'Not in ' + group

    def join_group(self, group):
        if not group in self.__joined_groups:
            try:
                mreq = struct.pack('4sl', socket.inet_aton(socket.gethostbyname(group)), socket.INADDR_ANY)
                port_socket = self.__open_mcast_socket(group)
                port_socket[1].setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
                self.__joined_groups[group] = port_socket
                return 'OK'
            except socket.error:
                return 'Fail'            
        else:
            return 'Already joined ' + group

    def get_joined_groups(self):
        return [('all', self.__current_group == None)] + [(group, self.__current_group == group) for group in self.__joined_groups]

    def leave_group(self, group):
        if group in self.__joined_groups:
            if group == self.__current_group:
                print 'Switching to all before leaving current group'
                self.switch_group('all')

            self.__joined_groups[group][1].close()
            self.__joined_groups.pop(group, None)
            return 'OK'
        else:
            return 'Not in ' + group

    def __process_socket(self, group, socket_to_process):
        msg, addr = socket_to_process.recvfrom(self.__buffer_size)

        if ord(msg[0]) == CLIENT_CMD:
            return self.__process_client_cmd(msg[1:], addr, socket_to_process)
        elif ord(msg[0]) == SIMPLE_TEXT:
            return { 'contents': msg[1:], 'sender': addr, 'group': group }
        

    def recv_msg(self):
        try:
            return self.__process_socket(None, self.__chat_socket)
        except:
            for group in self.__joined_groups:
                try:
                    return self.__process_socket(group, self.__joined_groups[group][1])
                except:
                    pass
            return None
