#!/usr/bin/python
import os, sys, socket, struct, select, time
from impacket import ImpactPacket, ImpactDecoder

MAX_PACKET_SIZE = 1024
used_ids = { }

def receive_one_ping(ping_socket, id, timeout):
    """
    Receive the ping response from the socket.
    """
    timeLeft = timeout
    while True:
        startedSelect = time.time()
        whatReady = select.select([ping_socket], [], [], timeLeft)

        howLongInSelect = (time.time() - startedSelect)
        if whatReady[0] == []: # Timeout
            return

        timeReceived = time.time()
        recPacket, addr = ping_socket.recvfrom(MAX_PACKET_SIZE)
        icmp_decoder = ImpactDecoder.ICMPDecoder()
        icmp_header = icmp_decoder.decode(recPacket[20:])

        if icmp_header.get_icmp_type() == icmp_header.ICMP_TIMXCEED:
            ip_decoder = ImpactDecoder.IPDecoder()
            ip_header = ip_decoder.decode(recPacket)
            addr = ip_header.get_ip_src()
            try:
                host = socket.gethostbyaddr(addr)[0]
            except:
                host = addr
            if (addr != host):
                print '* %s (%s)' % (host, addr)
            else:
                print '* %s' % addr
            return -1

        if icmp_header.get_icmp_id() == id:
            packet_sent_at = struct.unpack('d', icmp_header.get_data_as_string())[0]
            return timeReceived - packet_sent_at
 
        timeLeft = timeLeft - howLongInSelect
        if timeLeft <= 0:
            return
 
 
def send_one_ping(ping_socket, src_addr, dst_addr, id, ttl):
    """
    Send one ping to the given >dest_addr<.
    """
    if src_addr != 'auto':
        src_ip = socket.gethostbyname(src_addr)
    dst_ip = socket.gethostbyname(dst_addr)
    ip_packet = ImpactPacket.IP()
    
    if src_addr != 'auto':
        ip_packet.set_ip_src(src_ip)
    ip_packet.set_ip_dst(dst_ip)
    ip_packet.set_ip_ttl(ttl)

    icmp_packet = ImpactPacket.ICMP()
    icmp_packet.set_icmp_type(icmp_packet.ICMP_ECHO)
    icmp_packet.contains(ImpactPacket.Data(struct.pack('d', time.time())))
    icmp_packet.set_icmp_id(id)
    icmp_packet.set_icmp_cksum(0)
    icmp_packet.auto_checksum = True

    ip_packet.contains(icmp_packet)
    ping_socket.sendto(ip_packet.get_packet(), (dst_addr, 0))
 
def do_one(src_addr, dest_addr, timeout, ttl):
    """
    Returns either the delay (in seconds) or none on timeout.
    """
    try:
        ping_socket = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_ICMP)
        ping_socket.setsockopt(socket.IPPROTO_IP, socket.IP_HDRINCL, 1)
    except socket.error, (errno, msg):
        # Operation not permitted exception
        if errno == 1:
            msg = msg + (
                " - Note that ICMP messages can only be sent from processes"
                " running as root."
            )
            raise socket.error(msg)
        raise
 
    packet_id = os.getpid() & 0xFFFF
    while packet_id in used_ids:
        packet_id += 1

    send_one_ping(ping_socket, src_addr, dest_addr, packet_id, ttl)
    delay = receive_one_ping(ping_socket, packet_id, timeout)
    
    used_ids.pop(packet_id, None)
    ping_socket.close()
    return delay
 
 
def verbose_ping(src_addr, dest_addr, timeout = 2, count = 4, ttl = 30, traceroute_mode = False):
    """
    Send >count< ping to >dest_addr< with the given >timeout< and display
    the result.
    """
    for i in xrange(count):
        try:
            delay = do_one(src_addr, dest_addr, timeout, ttl)
        except socket.gaierror, e:
            print "failed. (socket error: '%s')" % e[1]
            return False
            
        if delay  ==  None:
            if traceroute_mode:
                print '*'
            else:
                print "failed. (timeout within %ssec.)" % timeout
            return False
        elif delay > 0:
            delay  =  delay * 1000
            print "Ping (from: %s; to: %s) in %0.4fms" % (src_addr, dest_addr, delay)
            return True
            
    return False


def do_traceroute(dst_addr, timeout = 5):
    print 'Tracing teh route to %s' % dst_addr
    for i in xrange(1, 30):
        print 'ttl = ', i
        if (verbose_ping('auto', dst_addr, timeout, count = 1, ttl = i, traceroute_mode = True)):
            print 'done!'
            return

if __name__ == '__main__':
    src_addr = 'auto'
    print 'Pinging stuff...'
    print 'Pinging google'
    verbose_ping(src_addr, 'google.com', timeout = 5, count = 1)
    print 'Pinging localhost'
    verbose_ping(src_addr, 'localhost')
    print 'Pinging google and spoofing'
    verbose_ping('google.com', '13.13.13.13', timeout = 10, ttl = 33)

    print
    do_traceroute('google.com')
