// MPI.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include <stdio.h>
#include <tchar.h>
#include <mpi.h>
#include <iostream>
#include <time.h>
#include <vector>
#define M 4
#define FILE
int compare (const void * a, const void * b)
{
  return ( *(int*)a - *(int*)b );
}

int* to1D(int** m)
{
	int* res = new int[M*M];
	for(int i = 0; i<M; i++)
		for(int j = 0; j<M; j++)
		{
			res[i*M+j] = m[i][j];
		}
	return res;
}

void print2D(int** m)
{
	for (int i=0; i < M; i++) 
	{
		for (int j=0; j < M; j++) 
			std::cout << m[i][j] << " ";
		std::cout << std::endl;
	}
}

#ifndef FILE
int main(int argc, char* argv[])
{
	std::vector<int> table;

	for(int i = 1; i<=M; i++)
		if(M % i == 0)
			table.push_back(i);
	
	MPI_Init(&argc, &argv);
	
	int globalRank, globalSize;
	
	MPI_Comm_rank(MPI_COMM_WORLD, &globalRank);
    MPI_Comm_size(MPI_COMM_WORLD, &globalSize);

	MPI_Comm comm;
	srand(time(NULL));
	int* groups = (int*)calloc(sizeof(int), globalSize);
	int numberOfGroups = atoi(argv[1]);
	
	for(int i = 1; i<=numberOfGroups;i++)
	{
		int count = table[rand() % (table.size()-1)];
		while(count > 0)
		{
			int randIndex = rand() % globalSize;
			if(groups[randIndex] == 0)
			{
				groups[randIndex] = i;
				count --;
			}
		}
	}	

	int color = groups[globalRank];

	MPI_Comm_split( MPI_COMM_WORLD, color, globalRank, &comm);
    
	if(color == 0)
	{
		MPI_Finalize();
		return 0;
	}
	
	int rank, size;
   
    MPI_Comm_rank(comm, &rank);
    MPI_Comm_size(comm, &size);

    int** A = new int*[M];
	int** B = new int*[M];
	for(int i(0); i<M; i++)
	{
		A[i] = new int[M];
		B[i] = new int[M];
		for (int j = 0; j < M; j++)
		{
			A[i][j] = i*M+j;
			B[i][j] = M*M-(i*M+j);
		}
	}
	int* C = new int[M*M];
	int* B1d = to1D(B);


	int perRank = (M*M/size);

	int* bufA = new int[perRank];
	int* bufC = (int*)calloc(sizeof(int), perRank);

	double startTime;  
	if (rank == 0) 
		startTime = MPI_Wtime();

	MPI_Scatter(to1D(A), perRank, MPI_INT, bufA, perRank, MPI_INT, 0, comm);
	MPI_Bcast(B1d, M*M, MPI_INT, 0, comm);

	for(int k = 0; k<perRank/M;k++)
		for(int i=0;i<M;i++)
		{
			bufC[k*M + i] = 0;
			for (int j = 0; j < M; j++)
				bufC[k*M + i] += bufA[k*M + j]*B1d[j*M+i];
		}
	

	MPI_Gather(bufC, perRank, MPI_INT, C, perRank, MPI_INT, 0, comm);	
    
	if(rank == 0 && color != 0)
		std::cout << "Group " << color << " (" << size << " processes) - " << MPI_Wtime() - startTime<< std::endl;
	MPI_Finalize();
	if(rank == 0 && color != 0)
	for (int i=0; i < M; i++) 
	{
		for (int j=0; j < M; j++) 
			std::cout << C[i*M+j] << " ";
		std::cout << std::endl;
	}
    return 0;
}
#else
int main(int argc, char* argv[])
{
	std::vector<int> table;

	for(int i = 1; i<=M; i++)
		if(M % i == 0)
			table.push_back(i);
	
	MPI_Init(&argc, &argv);
	
	int globalRank, globalSize;
	
	MPI_Comm_rank(MPI_COMM_WORLD, &globalRank);
    MPI_Comm_size(MPI_COMM_WORLD, &globalSize);

	MPI_Comm comm;
	srand(time(NULL));
	int* groups = (int*)calloc(sizeof(int), globalSize);
	int numberOfGroups = atoi(argv[1]);
	
	for(int i = 1; i<=numberOfGroups;i++)
	{
		int count = table[rand() % (table.size()-1)];
		while(count > 0)
		{
			int randIndex = rand() % globalSize;
			if(groups[randIndex] == 0)
			{
				groups[randIndex] = i;
				count --;
			}
		}
	}	

	int color = groups[globalRank];
	

	MPI_Comm_split( MPI_COMM_WORLD, color, globalRank, &comm);
    
	if(color == 0)
	{
		MPI_Finalize();
		return 0;
	}
	
	int rank, size;
   
    MPI_Comm_rank(comm, &rank);
    MPI_Comm_size(comm, &size);
	
	int** B = new int*[M];
	for(int i(0); i<M; i++)
	{
		B[i] = new int[M];
		for (int j = 0; j < M; j++)
		{
			B[i][j] = j;
		}
	}
	int* C = new int[M*M];
	int* B1d = to1D(B);


	int perRank = (M*M/size);

	int* bufA = new int[perRank];
	int* bufC = (int*)calloc(sizeof(int), perRank);

	double startTime;  
	if (rank == 0) 
		startTime = MPI_Wtime();
	
	

	MPI_File Afile;
	auto error = MPI_File_open(comm, "A.txt", MPI_MODE_RDONLY, MPI_INFO_NULL, &Afile);
	MPI_File_set_view(Afile, perRank*rank*sizeof(int), MPI_INT, MPI_INT, "native", MPI_INFO_NULL);
	MPI_File_read_all(Afile, bufA, perRank, MPI_INT, MPI_STATUSES_IGNORE); 
	MPI_File_close(&Afile);

	MPI_File Bfile;
	MPI_File_open(comm, "B.txt", MPI_MODE_RDONLY, MPI_INFO_NULL, &Bfile);
	MPI_File_read_all(Bfile, B1d, M*M, MPI_INT, MPI_STATUSES_IGNORE); 
	MPI_File_close(&Bfile);

	for(int k = 0; k<perRank/M;k++)
		for(int i=0;i<M;i++)
		{
			bufC[k*M + i] = 0;
			for (int j = 0; j < M; j++)
				bufC[k*M + i] += bufA[k*M + j]*B1d[j*M+i];
		}
	
	MPI_File Cfile;
	char buf[10] = "";
	sprintf_s(buf, "%d.txt\0", color);
	auto a = MPI_File_open(comm, buf, MPI_MODE_RDWR | MPI_MODE_CREATE, MPI_INFO_NULL, &Cfile);
	MPI_File_set_view(Cfile, perRank*rank*sizeof(int), MPI_INT, MPI_INT, "native", MPI_INFO_NULL);
	MPI_File_write_all(Cfile, bufC, perRank, MPI_INT, MPI_STATUSES_IGNORE); 
	MPI_File_close(&Cfile);
    
	if(rank == 0 && color != 0)
		std::cout << "Group " << color << " (" << size << " processes) - " << MPI_Wtime() - startTime<< std::endl;
	MPI_Finalize();
    return 0;
}

#endif