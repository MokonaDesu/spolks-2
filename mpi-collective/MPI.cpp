// MPI.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include <stdio.h>
#include <tchar.h>
#include <mpi.h>
#include <iostream>
#define M 4

int* to1D(int** m)
{
	int* res = new int[M*M];
	for(int i = 0; i<M; i++)
		for(int j = 0; j<M; j++)
		{
			res[i*M+j] = m[i][j];
		}
	return res;
}

void print2D(int** m)
{
	for (int i=0; i < M; i++) 
	{
		for (int j=0; j < M; j++) 
			std::cout << m[i][j] << " ";
		std::cout << std::endl;
	}
}

int main(int argc, char* argv[])
{
	MPI_Init(&argc, &argv);
   
    int rank, size;
   
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    int** A = new int*[M];
	int** B = new int*[M];
	for(int i(0); i<M; i++)
	{
		A[i] = new int[M];
		B[i] = new int[M];
		for (int j = 0; j < M; j++)
		{
			A[i][j] = i*M+j;
			B[i][j] = M*M-(i*M+j);
		}
	}
	int* C = NULL;

	if(rank == 0)
	{
		C = new int[M*M];
		for(int i = 1; i < size; i++)
		{
			MPI_Send(to1D(B), M*M, MPI_INT, i, 0, MPI_COMM_WORLD);
			MPI_Send(A[i], M, MPI_INT, i, 0, MPI_COMM_WORLD);
			
		}

		for(int i=0;i<M;i++)
		{
			C[i] = 0;
			for (int j = 0; j < M; j++)
				C[i] += A[0][j]*B[j][i];
		}

		int N = M;
		while(N != 1)
		{
			MPI_Status st;
			MPI_Request req;
			int* c = new int[M];
			
			MPI_Recv(c, M, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &st);
			for(int j = 0; j<M;j++)
				C[st.MPI_SOURCE*M+j] = c[j];
			delete[] c;
			N--;
		}

	} else
	{
		int* B = new int[M*M];
		MPI_Status st;
		MPI_Recv(B, M*M, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &st);
		int* A = new int[M];
		MPI_Recv(A, M, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &st);
		int* c = new int[M];
		for(int i=0;i<M;i++)
		{
			c[i] = 0;
			for (int j = 0; j < M; j++)
				c[i] += A[j]*B[j*M+i];
		}
		MPI_Send(c, M, MPI_INT, 0, 0, MPI_COMM_WORLD);
	}
    MPI_Finalize();
	if(rank == 0)
	for (int i=0; i < M; i++) 
	{
		for (int j=0; j < M; j++) 
			std::cout << C[i*M+j] << " ";
		std::cout << std::endl;
	}
    return 0;
}

