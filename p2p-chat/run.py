#!/usr/bin/python

import sys
import select
import chat
import ifacehelper
import socket

def get_line():
    i, o, e = select.select([sys.stdin], [], [], 0.1)
    for s in i:
        if s == sys.stdin:
            input = sys.stdin.readline()
            return input
    return None

mcast_port = 27016
if len(sys.argv) > 2 and (sys.argv[1] == '-m' or sys.argv[1] == '--mcast-base'):
    mcast_port = int(sys.argv[2])

client = chat.ChatClient(ifacehelper.choose_iface(), 27015, mcast_port, 13)

def username(addr):
    try:
        return socket.gethostbyaddr(addr)[0]
    except:
        return addr

def loop_chat():
    msg = client.recv_msg()
    if msg:
        current_group_tag = ''
        if 'group' in msg and msg['group'] != None:
            current_group_tag = '\033[93m@%s\033[0m' % msg['group']
        print '\033[4m\033[1m\033[91m%s\033[0m%s says: %s' % (username(msg['sender'][0]), current_group_tag, msg['contents'])

    input = get_line()
    if input != None:
        input = input[:len(input) - 1]
        if input == '!who':
            client.send_cmd(chat.IP_ECHO)
        elif (input.startswith('!join ')):
            group = input[6:]
            print 'Joining %s... %s' % (group, client.join_group(group))
        elif (input.startswith('!leave ')):
            group = input[7:]
            print 'Leaving %s... %s' % (group, client.leave_group(group))
        elif (input.startswith('!switch ')):
            group = input[8:]
            print 'Switching to %s... %s' % (group, client.switch_group(group))
        elif (input == '!groups'):
            for group in client.get_joined_groups():
                if (group[1]):
                    print '* \033[4m\033[1m\033[91m%s\033[0m' % group[0]
                else:
                    print '* %s' % group[0]
        else:
            client.send_msg(input)

while True:
    try:
        loop_chat()
    except KeyboardInterrupt:
        print ' Sayonara...'
        break
