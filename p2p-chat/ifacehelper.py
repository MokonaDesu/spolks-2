import netifaces
import socket

def choose_iface():
    ifaces = netifaces.interfaces()
    iface_details = [(iface, netifaces.ifaddresses(iface)) for iface in ifaces]
    valid_ifaces = [(iface[0], iface[1][socket.AF_INET][0]) for iface in iface_details if socket.AF_INET in iface[1] and 'broadcast' in iface[1][socket.AF_INET][0]]
    valid_ifaces = [{'name': iface[0], 'addr': iface[1]['addr'], 'bcast': iface[1]['broadcast']} for iface in valid_ifaces]

    if len(valid_ifaces) == 0:
        print 'No broadcast-capable interfaces found. Exiting...'
        return None

    if len(valid_ifaces) == 1:
        print 'Automatically assigned \033[4m\033[1m\033[91m%s\033[0m as primary interface.' % valid_ifaces[0]['name']
        return valid_ifaces[0]
    else:
        for index, iface in enumerate(valid_ifaces):
            print '%d: %s (addr: %s; bcast: %s)' % (index, iface['name'], iface['addr'], iface['bcast'])
        while True:
            try:
                option = int(raw_input('Choose an interface (0..%d): ' % (len(valid_ifaces) - 1)))
                print 'Assigned \033[4m\033[1m\033[91m%s\033[0m as primary interface.' % valid_ifaces[option]['name']
                return valid_ifaces[option]
            except:
                pass
